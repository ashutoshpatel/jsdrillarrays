// create a reduce function
function reduce(elements, cb, startingValue) {

    // if the elements length is 0 means it's empty
    if(elements.length == 0){
        return null;
    }

    // iterate on array
    for(let index=0; index<elements.length; index++){
        // call a callback function and update the startingValue
        startingValue = cb(startingValue , elements[index]);
    }

    // return the startingValue
    return startingValue;
}

// export the reduce function
module.exports = reduce;