// create flatten function
function flatten(elements) {

    // create empty array to store result
    const arr = [];

    // if the elements length is 0 means it's empty
    if(elements.length == 0){
        return arr;
    }

    // create helperFunction which at like recursion
    function helperFunction(currentArray){

        // iterate on array
        for(let index=0; index <  currentArray.length; index++){

            // if the current index is also have a array
            if(Array.isArray(currentArray[index])){
                // then call a helperFunction
                helperFunction(currentArray[index]);
            }
            else{
                // if the current index is value then push into array
                arr.push(currentArray[index]);
            }
        }
    }

    // call helperFunction
    helperFunction(elements);
    // return the arr array
    return arr;
}

// export the flatten function
module.exports = flatten;