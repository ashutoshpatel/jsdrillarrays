// import map function
const map = require('../map.js');

const items = [1, 2, 3, 4, 5, 5];

// call map function
const result = map(items, (value) => {
    // if the element is greater than 2
    if(value >= 2){
        // then return with the modification
        return "the value is " + value;
    }
})

if(result.length != 0){
    // print the output
    console.log(result);
}
else{
    // means the array is empty
    console.log("Array is empty");
}