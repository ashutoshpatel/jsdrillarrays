// import the flatten function
const flatten = require('../flatten.js');

const nestedArray = [1, [2], [[3]], [[[4]]]];

// call the flatten function
const result = flatten(nestedArray);

if(result.length != 0){
    // print the output
    console.log(result);
}
else{
    // means the array is empty
    console.log("Array is empty");
}