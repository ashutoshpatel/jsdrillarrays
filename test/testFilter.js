// import the filter function
const filter = require('../filter.js');

const items = [1, 2, 3, 4, 5, 5];

// call the filter function
const result = filter(items , (value) => {
    // if the current value is greater than 2 return true
    return value >= 2;
});

if(result.length != 0){
    // print the output
    console.log(result);
}
else{
    // means the array is empty
    console.log("Array is empty");
}