// import find function
const find = require('../find.js');

const items = [1, 2, 3, 4, 5, 5];

// call find function
const result = find(items , (value) => {
    // return true if the value is greater then 4
    return value > 4;
});

if(result === null){
    // means the array is empty
    console.log("Array is empty");
}
else{
    // print the output
    console.log(result);
}