// import the reduce function
const reduce = require('../reduce.js');

const items = [1, 2, 3, 4, 5, 5];

// call the reduce function
const result = reduce(items, function(acc, current){
    return current + acc;
}, 0);

if(result !== null){
    // print the output
    console.log(result);
}
else{
    // means the array is empty
    console.log("Array is empty");
}