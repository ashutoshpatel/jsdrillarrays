// import the each function
const each = require('../each.js');

const items = [1, 2, 3, 4, 5, 5];

// call the each function
let result = each(items, (value, index) => {
    // print the output
    console.log("Index is : " + index + " and element is : " + value);
})

if(result ===  null){
    // means the array is empty
    console.log("Array is empty");
}