// create a filter function
function filter(elements, cb) {

    // create empty array to store filter result
    const arr = [];
    let answer = false;

    // if the elements length is 0 means it's empty
    if(elements.length == 0){
        return arr;
    }

    // iterate on array
    for(let index=0; index < elements.length; index++){
        // call the callback function
        answer = cb(elements[index]);

        // if the answer is two means we got truth value
        if(answer){
            // so add the elements in arr array
            arr.push(elements[index]);
        }
    }

    // return the arr array
    return arr;
}

// export the filter function
module.exports = filter;