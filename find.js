// create find function
function find(elements, cb) {

    // if the elements length is 0 means it's empty
    if(elements.length == 0){
        return null;
    }

    let answer = false;

    // iterate on array
    for(let index=0; index < elements.length; index++){
        
        // call the callback function
        answer = cb(elements[index]);

        // if the answer is true means we got result
        if(answer){
            // so return the current value
            return elements[index];
        }
    }

    // return undefined if the any element not pass truth value
    return undefined;
}

// export the find function
module.exports = find;