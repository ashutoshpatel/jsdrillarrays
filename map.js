// create a map function
function map(elements, cb) {
    
    // create empty array
    const arr = [];

    // if the elements length is 0 means it's empty
    if(elements.length == 0){
        return arr;
    }

    // iterate on a elements array
    for(let index=0; index < elements.length; index++){
        // call a callback function
        arr[index] = cb(elements[index]);
    }
    // return the array
    return arr;
}

// export map function
module.exports = map;