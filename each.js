// create each function
function each(elements, cb) {

    // if the elements length is 0 means it's empty
    if(elements.length == 0){
        return null;
    }

    // iterate on elements
    for(let index=0; index < elements.length; index++){
        // call the callback function
        cb(elements[index], index);
    }

    return 0;
}

// export the each function
module.exports = each;